#include "DHT.h"
#include <SPI.h>
#include <SD.h>

const int chipSelect = 4; // Pino CS do Leitor de Cartão SD

#define DHTPIN 7
#define DHTTYPE DHT11   // DHT 11

// Instancia o objeto DHT
DHT dht(DHTPIN, DHTTYPE);

void setup(){
  Serial.begin(9600);
  Serial.print("Inicializando Cartão SD...");
  // Verifica o Cartão
  if (!SD.begin(chipSelect)) {
    Serial.println("O cartão falhou.");
    return;
  }
  Serial.println("Cartão Incializado.");
  Serial.println("Sensor de Temperatura");
  dht.begin();
    delay(10);
  }



void loop(){

  float h = dht.readHumidity();
  float t = dht.readTemperature();
  // Cálcula o a sensação térmica com base na Temperatura e na Umidade, (isFahrehghheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Umidade: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.println("Temperatura: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.println("Sensacao Termica: ");
  Serial.print(hic);
  Serial.print(" *C ");

  //LogToSD();

  delay(2000);
}

/*void LogToSD(){
  File dataFile = SD.open("datalog.csv", FILE_WRITE);

  // Verifica se o arquivo datalog.csv está disponivel
  if(dataFile){
    dataFile.print(h);
    dataFile.print(",");
    dataFile.print(t);
    dataFile.print(",");
    dataFile.print(hic);
    dataFile.close();
  }
  else{
    Serial.println("ERRO AO ABRIR O ARQUIVO datalog.csv");
  }
}*/
